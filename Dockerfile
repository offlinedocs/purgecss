# Build
FROM node:latest AS builder

RUN git clone https://github.com/FullHuman/purgecss.git /purgecss

WORKDIR /purgecss

RUN npm install
RUN npm run docs:build

# Serve
FROM nginx:alpine

COPY --from=builder /purgecss/docs/.vuepress/dist /usr/share/nginx/html
